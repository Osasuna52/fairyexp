﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FairyExp
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Height = 130;
            this.Width = 520;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FairyCalc(textBox1.Text, textBox2.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private int ToGet(string CL, string DL)
        {
            int currentLevel = 0;
            int destinatedLevel = 0;
            bool Valid = false;
            int Number;
            while (Valid == false) //check if inputs are integers not letters or floats
            {
                string currentLev = CL;
                string destinatedLev = DL;

                if (int.TryParse(currentLev, out Number) && int.TryParse(destinatedLev, out Number))
                {
                    Valid = true;
                    currentLevel = int.Parse(CL);
                    destinatedLevel = int.Parse(DL);
                }
                else
                {
                    MessageBox.Show("Wrong numbers or missing arguments!");
                    break;
                }
            }


            int toGet = 0;
            if (currentLevel >= 0 && currentLevel <= 80 && destinatedLevel >= 0 && destinatedLevel <= 80) // checks if input is in desired scope (for all faries 0 - 80)
            {
                for (int i = currentLevel; i < destinatedLevel; i++)
                {
                    if (i < 40)
                        toGet += i * i + 50;
                    else if (i >= 40 && i <= 80)
                        toGet += (i * i + 50) * 3;
                }
            }
            else
            {
                // MessageBox.Show("Wrong numbers!");
            }
            /* SIMPLIER CALCULATOR */
            return toGet;
        }

        private void FairyCalc(string CurrentLevel, string DestinatedLevel)
        {
            /* SIMPLIER CALCULATOR */
            int toGet = ToGet(CurrentLevel, DestinatedLevel);
            textBox3.Text = (toGet).ToString();



        }

        private void AdvancedCalc(string curLevel, string desLevel, string killsPerMinute, string hours, bool checkbox)
        {
            /* Numbers check */ 
            int currentLevel = 0;
            int destinatedLevel = 0;
            int killsPerM = 0;
            int hoursCheck = 0;
            bool Valid = false;
            int Number;
            while (Valid == false) //check if inputs are integers not letters or floats
            {
                string currentLev = curLevel;
                string destinatedLev = desLevel;
                string killsPM = killsPerMinute;
                string hoursCh = hours;

                if (int.TryParse(currentLev, out Number) && int.TryParse(destinatedLev, out Number) && int.TryParse(killsPM, out Number) && int.TryParse(hoursCh, out Number))
                {
                    Valid = true;
                    currentLevel = int.Parse(curLevel);
                    destinatedLevel = int.Parse(desLevel);
                    killsPerM = int.Parse(killsPerMinute);
                    hoursCheck = int.Parse(hours);
                }
                else
                {
                    //MessageBox.Show("Wrong numbers or missing arguments!");
                    break;
                }
            }
            /* Numbers check */

            /* ADVANCED CALCULATOR */
            if (currentLevel >= 0 && currentLevel <= 80 && destinatedLevel >= 0 && destinatedLevel <= 80 && killsPerM != 0) // checks if input is in desired scope (for all faries 0 - 80)
            {
                float reachInXHours = 0;
                int youLLGet = 0;
                int toGet = ToGet(curLevel, desLevel);
                float toGetF = (float)toGet;
                if(checkBox1.Checked == true)
                {
                    reachInXHours = toGetF / 2 / (float)killsPerM / 60; // calcs how many hours player need to get destinated fairy lvl with specified kills per min
                }
                else
                {
                    reachInXHours = toGetF / (float)killsPerM / 60; // calcs how many hours player need to get destinated fairy lvl with specified kills per min
                }
                if (checkBox1.Checked == true)
                {
                    youLLGet = killsPerM * 60 * hoursCheck * 2; //calc how many monsters player will kill with specfied time and kill per min
                }
                else
                {
                    youLLGet = killsPerM * 60 * hoursCheck; //calc how many monsters player will kill with specfied time and kill per min
                }
                if (Valid == true && hoursCheck != 0)
                {
                    for (int i = currentLevel; i < 80; i++) // calculate how many % your fiary will get in specified amount of time
                    {
                        if (ToGet(currentLevel.ToString(), i.ToString()) > youLLGet)
                        {
                            textBox9.Text = (i - 1).ToString();
                            i = 80;
                        }
                    }
                }
                else
                {
                }

                textBox8.Text = reachInXHours.ToString();
            }
            else
            {
                MessageBox.Show("Wrong numbers!");
            }


            /* ADVANCED CALCULATOR */
        }

        private void button2_Click(object sender, EventArgs e) // change window width to show right panel
        {
            Form2 f2 = new Form2();
            if(FormIsOpen(Application.OpenForms, typeof(Form2)) == true) // doesn't allow to open multiple form2
            {
                f2.Hide();
            }
            else
            {
                f2.Show();
            }
        }

        public static bool FormIsOpen(FormCollection application, Type formType) //used to check if form is opened
        {
            //usage sample: FormIsOpen(Application.OpenForms,typeof(Form2)
            return Application.OpenForms.Cast<Form>().Any(openForm => openForm.GetType() == formType);
        }

        private void button3_Click(object sender, EventArgs e) // show additional panel (not needed)
        {
            if(this.Height == 312)
            {
                this.Height = 130;
            }
            else
            {
                this.Height = 312;
                this.Height = 312;
            }
            textBox7.Text = 0.ToString();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            AdvancedCalc(textBox4.Text, textBox5.Text, textBox6.Text, textBox7.Text, checkBox1.Checked);
        }
    }



}
