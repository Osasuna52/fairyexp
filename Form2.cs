﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FairyExp
{
    public struct Monsters // structs for monsters: image + hp + exp etc
    {
        public Bitmap monImage;
        public string monName;
        public int monLevel, monMax;

        public Monsters(Bitmap monsterImage, string monsterName, int monsterLevel, int monsterMax)
        {
            monImage = monsterImage;
            monName = monsterName;
            monLevel = monsterLevel;
            monMax = monsterMax;
        }

    }

    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Paint += new PaintEventHandler(Form2_Paint);
            pictureBox1.Paint += new PaintEventHandler(PictureBox1_Paint);

            this.Width = 500;
            this.Height = 425;
            this.DoubleBuffered = true;
            //this.SetAutoScrollMargin(0, 500);
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            this.DoubleBuffered = true;

           
            
            pictureBox1.Parent = this;
            
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawString("Monster               Level              Max hit level", this.Font, Brushes.Black, 25, 5);

            Monsters monster1 = new Monsters(Properties.Resources._24, "Big Dander", 4, 19); 
            Monsters monster2 = new Monsters(Properties.Resources._30, "Seedle", 5, 20); 
            Monsters monster3 = new Monsters(Properties.Resources._36, "Soft Pii", 8, 23);
            Monsters monster4 = new Monsters(Properties.Resources._45, "Soft Pii-pod", 8, 23); 
            Monsters monster5 = new Monsters(Properties.Resources._37, "Pii", 10, 25); 
            Monsters monster6 = new Monsters(Properties.Resources._45, "Pii-pod", 10, 25); 
            Monsters monster7 = new Monsters(Properties.Resources._37, "Hard Pii", 13, 28); 
            Monsters monster8 = new Monsters(Properties.Resources._45, "Hard Pii-pod", 13, 28); 

            int spaceX = 0;
            int spaceY = 0;
            e.Graphics.DrawImage(monster1.monImage, spaceX + 30, spaceY + 15);
            e.Graphics.DrawString(monster1.monName.ToString(), this.Font, Brushes.Black, spaceX + 15, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster1.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 30, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster1.monMax.ToString(), this.Font, Brushes.Black, spaceX + 25, spaceY + monster4.monImage.Height + 25);
            spaceX += 100;
             e.Graphics.DrawImage(monster2.monImage, spaceX + 15, spaceY + 30);
            e.Graphics.DrawString(monster2.monName.ToString(), this.Font, Brushes.Black, spaceX + 25, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster2.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 30, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster2.monMax.ToString(), this.Font, Brushes.Black, spaceX + 25, spaceY + monster4.monImage.Height + 25);
            spaceX += 100;
             e.Graphics.DrawImage(monster3.monImage, spaceX + 35, spaceY + 30);
            e.Graphics.DrawString(monster3.monName.ToString(), this.Font, Brushes.Black, spaceX + 45, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster3.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 50, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster3.monMax.ToString(), this.Font, Brushes.Black, spaceX + 45, spaceY + monster4.monImage.Height + 25);
            spaceX += 100;
             e.Graphics.DrawImage(monster4.monImage, spaceX + 40, spaceY);
            e.Graphics.DrawString(monster4.monName.ToString(), this.Font, Brushes.Black, spaceX + 55, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster4.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 70, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster4.monMax.ToString(), this.Font, Brushes.Black, spaceX + 65, spaceY + monster4.monImage.Height + 25);
            spaceX = 0;
            spaceY += monster4.monImage.Height+45;
            e.Graphics.DrawImage(monster5.monImage, spaceX + 15, spaceY + 30);
            e.Graphics.DrawString(monster5.monName.ToString(), this.Font, Brushes.Black, spaceX + 38, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster5.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 30, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster5.monMax.ToString(), this.Font, Brushes.Black, spaceX + 25, spaceY + monster4.monImage.Height + 25);
            spaceX += 100;
            e.Graphics.DrawImage(monster6.monImage, spaceX + 20, spaceY);
            e.Graphics.DrawString(monster6.monName.ToString(), this.Font, Brushes.Black, spaceX + 43, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster6.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 45, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster6.monMax.ToString(), this.Font, Brushes.Black, spaceX + 40, spaceY + monster4.monImage.Height + 25);
            spaceX += 100;
            e.Graphics.DrawImage(monster7.monImage, spaceX + 35, spaceY + 30);
            e.Graphics.DrawString(monster7.monName.ToString(), this.Font, Brushes.Black, spaceX + 45, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster7.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 50, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster7.monMax.ToString(), this.Font, Brushes.Black, spaceX + 45, spaceY + monster4.monImage.Height + 25);
            spaceX += 100;
            e.Graphics.DrawImage(monster8.monImage, spaceX + 40, spaceY);
            e.Graphics.DrawString(monster8.monName.ToString(), this.Font, Brushes.Black, spaceX + 50, spaceY + monster4.monImage.Height + 5);
            e.Graphics.DrawString("Lvl: " + monster8.monLevel.ToString(), this.Font, Brushes.Black, spaceX + 70, spaceY + monster4.monImage.Height + 15);
            e.Graphics.DrawString("Max: " + monster8.monMax.ToString(), this.Font, Brushes.Black, spaceX + 65, spaceY + monster4.monImage.Height + 25);


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
